function josephus(items, k) {
    let lap = items.length;
    let reslt = [];
//    while (ci.length > 0) {
        reslt.push(...items.splice(k - 1, 1));
        reslt.push(...items.splice(k - 1, 1));
        reslt.push(...items.splice(k - 1, 1));
        reslt.push(...items.splice(k - 1, 1));
//        console.log(ci);
//    }
    return items.length;
}

console.log(josephus([1, 2, 3, 4, 5, 6, 7], 3))

/*
function SeriesSum(n){
    let cur3 = 1;
    let curSum = 0;
    for(let j = 0; j<n; j++){
        curSum += 1/cur3
        cur3+=3;
        console.log(curSum)
    }
    
    let round2 = curSum.toFixed(2);
    
    return String(round2);
}
//Test.assertEquals(SeriesSum(1), "1.00")
//Test.assertEquals(SeriesSum(2), "1.25")
console.log(`result = ${SeriesSum(6)}`); //"1.39" 
    //////////////
    ////// have two contenders fight and declare a winner
    //////////////
    function declareWinner(fighter1, fighter2, firstAttacker) {
    class Fighter {
        constructor(name, health, damagePerAttack) {
            this.name = name;
            this.health = health;
            this.damagePerAttack = damagePerAttack;
            this.toString = function () {
                return this.name;
            }
        }
    }        let atckr = fighter1.name === firstAttacker ? fighter1 : fighter2;
        let atckd = fighter1.name === firstAttacker ? fighter2 : fighter1;

        function fight() {
            if (atckd.health > 0 && atckr.health > 0) {
//                console.log(` attacker = ${atckr.name} life= ${atckr.health} \n attacked = ${atckd.name} life= ${atckd.health}`);
                atckd.health -= atckr.damagePerAttack;
            [atckd, atckr] = [atckr, atckd];
                fight();
            }
            return atckd.health <= 0 ? atckr.name : atckd.name;

        }
        return fight();
    }
    declareWinner(new Fighter("Lew", 10, 2), new Fighter("Harry", 5, 4), "Lew");
    function getBaseLog(x, y) {
      return Math.log(y) / Math.log(x);
    }

    // 2 x 2 x 2 = 8
    console.log(getBaseLog(2, 8));
    // expected output: 3

    // 5 x 5 x 5 x 5 = 625
    console.log(getBaseLog(5, 625));
    // expected output: 4
    function evaporator = (c, e, t) => {
        let ev = e / 100;
        let tot = 100;
        let days = 0;
        while (tot > t) {
            tot -= tot * ev
            days++
        }
        return days
    }
    //////////////
    ////// process the triangle according to the combinations (more efficient)
    //////////////
    
    function triangle(row) {
        const reR = /(?:BG|GB|RR)/;
        const reG = /(?:RB|BR|GG)/;
        const reB = /(?:RG|GR|BB)/;
        let res = row;
        function prc(p) {
            if (reR.test(p)) {
                return "R"
            }
            if (reG.test(p)) {
                return "G"
            }
            if (reB.test(p)) {
                return "B"
            }
        }
        function iter(rw) {
            let cuRow = "";
            for (let j = 0; j < rw.length - 1; j++) {
                cuRow += prc(rw.substring(j, j + 2));
            }
            res = cuRow;
        }
        while (res.length>1){
            iter(res);
        }
        return res;
        //    R B R G B R B 
        //     G G B R G G
        //      G R G B G
        //       B B R R
        //        B G R
        //         R B
        //          G
    }
    console.log(triangle("RGBRGBRRRGBBBGBRBGRGBRBRBBGRBRBGBRBGGRBGBGBGGGRBGR"))
    //////////////
    ////// process the riangle according to the combinations (inefficient)
    //////////////
    function triangle(row) {
        function proc(a, b) {
            if (a === b) {
                return a
            }
            if (a === "R" && b === "G" || a === "G" && b === "R") {
                return "B"
            }
            if (a === "B" && b === "G" || a === "G" && b === "B") {
                return "R"
            }
            if (a === "R" && b === "B" || a === "B" && b === "R") {
                return "G"
            }
        }

        function advRows(cRow) {
            console.log(cRow)
            if (cRow.length > 1) {
                itrt(cRow);
            } else {
                res = cRow[0] || row;
            }
        }

        function itrt(tRow) {
            let row1 = tRow;
            let row2 = [];
            for (let j = 0; j < tRow.length - 1; j++) {
                row2[j] = proc(tRow[j], tRow[j + 1])
            }
            advRows(row2);
        }
        let res = "";
        itrt(row);
        return res;
        //      Colour here:            G G        B G        R G        B R
        //      Becomes colour here:     G          R          B          G

        //    R B R G B R B 
        //     G G B R G G
        //      G R G B G
        //       B B R R
        //        B G R
        //         R B
        //          G
    }
    //////////////
    ////// Greet in a given language
    //////////////
    function greet(language) {
        let ldb = {
            english: 'Welcome',
            czech: 'Vitejte',
            danish: 'Velkomst',
            dutch: 'Welkom',
            estonian: 'Tere tulemast',
            finnish: 'Tervetuloa',
            flemish: 'Welgekomen',
            french: 'Bienvenue',
            german: 'Willkommen',
            irish: 'Failte',
            italian: 'Benvenuto',
            latvian: 'Gaidits',
            lithuanian: 'Laukiamas',
            polish: 'Witamy',
            spanish: 'Bienvenido',
            swedish: 'Valkommen',
            welsh: 'Croeso'
        }
        return ldb[language] || ldb.english;
    }
    console.log(greet("patois"))
    //////////////
    ////// Count vowels
    //////////////
    const getCount = (str) => (str.match(/[aeiou]+/g) || []).join("").length ;
    //////////////
    ////// Calculate the most efficient amount of fuel to make a given number of iron ingots on minecraft
    //////////////
    function calcFuel(n) {
        const ironS = 11;
        const time = [800, 120, 80, 15, 1];
        let seconds = ironS * n;
        let resources = [0, 0, 0, 0, 0];
        time.forEach(function (i, k) {
            while (seconds >= i) {
                resources[k]++;
                seconds -= i;
            }
        })
        let resObj = {
            lava: resources[0],
            blazeRod: resources[1],
            coal: resources[2],
            wood: resources[3],
            stick: resources[4]
        }
        return resObj;
    };

    //////////////
    ////// validate that a number has either exactly 4 or exactly 6 digits
    //////////////
    const validatePIN = (pin) => /(^\d{4}$|^\d{6}$)/.test(String(pin));

    //////////////
    ////// Calculate the number of times a number digits need to be multiplied to obtain a single digit
    //////////////
    function persistence(num) {
        const mult = (tot, cur) => tot * parseInt(cur);
        const sep = (n) => Array.from(String(n));
        let res = num;
        let ctr = 0;
        function calcPer() {
            if (res >= 10) {
                res = sep(res).reduce(mult);
                ctr++;
                calcPer();
            }
            return ctr;
        }
        return calcPer();
    }

    //////////////
    ////// Calculate the overall time a queue takes given a number of chechout points and times for each customer
    //////////////
    function queueTime(cust, n) {
        let elapsd = 0;
        let tills = new Array(n).fill(0);
        let curTill = 0;
        const pickTill = () => curTill = tills.indexOf(Math.min.apply(null, tills));
        while (cust.length > 0) {
            pickTill()
            tills[curTill] += cust.shift();
            if (tills[curTill] > elapsd) {
                elapsd = tills[curTill]
            }
        }
        return elapsd
    }
    //////////////
    ////// Roll a certain number of dice of a certain number of sides  a certain number of
    ////// times and obtain statistics about the results mapped in an ascii histogram.
    //////////////

    function histogram(res) {
        let inv = res.reverse();
        let hstg = "";
        for (let j in inv) {
            hstg += inv.length - parseInt(j) + "|";

            for (let i = 0; i < res[j]; i++) {
                hstg += "#"
            }
            if (inv[j] != 0) {
                hstg += " " + inv[j]
            }
            hstg += "\n";

        }
        return hstg

    }
    function roll(sides,times){
        let res = new Array(sides).fill(0);
    //    res.forEach(el => el=0);
    //    console.log("")
        for(let i=0; i<times; i++){
           res[Math.floor(Math.random()*sides)] ++
        }
        return res
    }
    //////////////
    ////// merge arrays and sort the resulting array
    //////////////

    function flattenAndSort(arry) {
        let mergd = [];
        for (let j in arry) {
            let tmp = arry[j]
            mergd.push(...tmp)
        };
        return uniqd.sort((a,b)=> a-b);
    };
    //////////////
    ////// "find the largest number among a srting"
    //////////////

    function solve(s) {
        let str = s;
        let tst = /(\d+)/g;
        let res = [];
        let itr = [];
        while ((itr = tst.exec(str)) !== null) {
                res.push(itr[0]);
            }
            return Math.max(...res)
        };
    //////////////
    ////// "Count Sheep"
    //////////////
        function stringy(size) {
            let res = "";
            for (let j = 1; j < size + 1; j++) {
                res += j + " Sheep...";
            }
            return res
        }


        //////////////
        ////// Return x amount of 1s and 0s
        //////////////

        function stringy(size) {
            let res="";
            let stt=true;
            for (let j = 0; j < size; j++) {
                if(stt){
                    res+="1";
                    stt = !stt;
                }
                else{
                    res+="0";
                    stt = !stt;
                }
            }
            return res
        }

        //////////////
        ////// Return highest an lowest numbers
        //////////////

        function highAndLow(numbers) {
            let reslt = 0;
            let ordrd = new Set(numbers.split(" ").map(x => Number(x)));
            reslt = String(Math.max(...ordrd));
            reslt += " " + String(Math.min(...ordrd));
            return reslt
        }

        //////////////
        ////// Reverse character order in individual words
        //////////////
        function reverseWords(str) {
          var inp = str.split(" ");
          var outr = [];
            for(j of inp){
               outr.push(j.split("").reverse().join(""));
            }
        //  for (var i = str.length - 1; i >= 0; i--)
        //    outr += str[i];
        return outr.join(" ");
        }

        //////////////
        ////// Generate characters to follow a pattern:  accum("abcd") -> "A-Bb-Ccc-Dddd"
        //////////////
        function accum (s) {
            let curStr="";
            for (let j=0; j<s.length; j++){
                curStr += s[j].toUpperCase();
                curStr += s[j].toLowerCase().repeat(j);
                if (j < s.length-1){curStr += "-"}
                
            }
            return curStr
        }
        //////////////
        ////// Calculate century from year
        //////////////
        const century = (year) => Math.ceil(year/100)

        //////////////
        ////// Write a function called repeatString which repeats the given String src exactly ////// count times.
        //////////////

        const repeatStr = (n, s) => s.padEnd(s.length*n,s)

        //////////////
        ////// Calculate the number of years required to reach a target ammount given an initial
        ////// amount and a set tax and interest
        //////////////
        function calculateYears(principal, interest, tax, desired) {
            let years = 0;
            let currentAmnt = principal;
            let thisYearInt = 0;
            let thisYearTax = 0;
            while (currentAmnt < desired) {
                years++;
                thisYearInt = currentAmnt * interest;
                thisYearTax = thisYearInt * tax;
                currentAmnt += thisYearInt - thisYearTax;

            }
            return years;
        }
        //////////////
        ////// Sum only the pusitive values of an array
        //////////////
        function positiveSum(arr) {

            let suma = 0;
            for( j of arr){
                if (j > 0){
                    suma += j;
                } 
            }
            return suma;
        }
        //////////////
        ////// Evaluate if a string is a palindrome
        //////////////

        let yesPal = "Are we not drawn onward to new era";
        //let yesPal = "Ar1ra";
        let noPal = "fjdljfaAefAjdkAffAm";

        // Check if string is a palindrome

        function isPalindrome(x) {
            function process(y) {
                let b = y.toLowerCase().split(" ");
                let c = "";
                for (j of b) {
                    c += j;
                }
                return c;
            }

            function check(st) {
                let processed = process(st);
                let curInv = 0;
                for (let l = 0; l < processed.length; l++) {
                    curInv = (processed.length - 1) - l;
                    if (processed.slice(l, l + 1) !== processed.slice(curInv, curInv + 1)) {
                        return false;
                    }
                    //            console.log("Letra: " + processed.slice(l, l + 1) + " posicion " + l);
                    //            console.log(" backwards " + processed.slice(curInv, curInv + 1));
                }
                return true;
            }
            return check(x);
        }
        console.log(isPalindrome(noPal))
        //////////////
        ////// get an array whose components are each individual word in a string
        //////////////

        const stringToArray = (strng)=> strng.split(" ")

        */
